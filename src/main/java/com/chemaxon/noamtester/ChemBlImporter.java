package com.chemaxon.noamtester;

import com.chemaxon.noam.importers.DocumentImporter;
import com.chemaxon.noam.importers.DocumentImporterContext;
import com.chemaxon.noam.model.ChemicalDocument;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChemBlImporter {
    
    private static final String CHEMBL_MRV_FOLDER_PATH = "/home/ksteindl/Documents/chembl/chembl-mrv";
    private static final Integer MOLECULE_COUNT = 100000;

    final static Logger logger = LoggerFactory.getLogger(ChemBlImporter.class);

    public static void main(String[] args) {
        createImportErrorReport();
    }

    public static void createImportErrorReport() {
        List<File> mrvFiles = getMrvFiles(CHEMBL_MRV_FOLDER_PATH, MOLECULE_COUNT);
        Map<String, List<File>> stacktraces = new HashMap<>();
        for (File mrvFile : mrvFiles) {
            try {
                importDocument(mrvFile);
            } catch (Exception exception) {
                String stacktrace = ExceptionUtils.getStackTrace(exception);
                if (stacktraces.get(stacktrace) == null) {
                    stacktraces.put(stacktrace, new ArrayList<>());
                }
                stacktraces.get(stacktrace).add(mrvFile);
            }
        }
        stacktraces.entrySet().forEach(entry -> {
            logger.info("Exception occured " + entry.getValue().size() + " times out of " + MOLECULE_COUNT + " molecules");
            logger.info("in the following MRV-s:");
            entry.getValue().stream().forEach(file -> logger.info(file.getName()));
            logger.info(entry.getKey());});
    }

    private static ChemicalDocument importDocument(File file) throws IOException {
        DocumentImporter importer = new DocumentImporter();
        DocumentImporterContext context = DocumentImporterContext
                .builder()
                .forcedToMRV()
                .build();
        ChemicalDocument importedDocument = importer.importDocument(file, context);
        return importedDocument;
    }

    public static List<File> getMrvFiles(final String folderPath, int count) {
        File folder = new File(folderPath);
        File[] files = folder.listFiles();
        int step = files.length /count;
        List<File> list = new ArrayList<>();
        for (int i = 0; i < files.length; i += step) {
            list.add(files[i]);
        }
        return list;
    }


}
